package com.babii.parser;

import com.babii.model.Plane;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JsonPlaneParser {
    private static Gson gson;

    public static List<Plane> getObjectFromJson(File file) {
        List<Plane> planes;
        String json = null;
        try {
            json = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        gson = new Gson();
        planes = Arrays.asList(gson.fromJson(json, Plane[].class));
        return planes;
    }

    public static boolean setObjectToJson(Object[] planes) {
        gson = new Gson();
        String json = gson.toJson(planes);
        try {
            FileUtils.write(new File("src/main/resources/plane1.json"), json, "UTF-8");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
